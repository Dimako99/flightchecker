﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightChecker.Models
{
    public class searchedItem : searchItem
    {
        public string price { get; set; }
        public string searchLocation { get; set; }
        public DateTime dateSearched { get; set; }
    }
}
