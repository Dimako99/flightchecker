﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightChecker.Models
{
    public class searchItem
    {
        public string name { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public DateTime departDate { get; set; }
        public DateTime returnDate { get; set; }
    }
}
