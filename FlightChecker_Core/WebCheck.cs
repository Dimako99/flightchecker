﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using FlightChecker.Models;
using System.Configuration;

namespace FlightChecker
{
    public class WebCheck
    {
        static readonly int sleepTime = int.Parse(ConfigurationSettings.AppSettings["sleepTime"]);

        public static void checkGoogleFlights(searchItem searchItem)
        {
            Console.WriteLine($"Checking flight prices for trip '{searchItem.name}' on Google Flights.");

            // Setup Web Driver
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--incognito");
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.SuppressInitialDiagnosticInformation = true;
            service.HideCommandPromptWindow = true;
            IWebDriver driver = new ChromeDriver(service, options);
            driver.Manage().Timeouts().PageLoad = new TimeSpan(0,0, 30);
            driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 10);
            driver.Manage().Window.Maximize();

            // Navigate to URL
            driver.Url = "https://flights.google.com";
            slowDown();

            // Set From
            driver.FindElement(By.XPath("//*[@id=\"flt-app\"]/div[2]/main[1]/div[3]/div/div[3]/div/div[2]/div[1]")).Click();
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sb_ifc50\"]/input")).Clear();
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sb_ifc50\"]/input")).SendKeys(searchItem.from);
            System.Threading.Thread.Sleep(500);
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sbse0\"]")).Click();
            System.Threading.Thread.Sleep(500);
            slowDown();

            // Set To
            driver.FindElement(By.XPath("//*[@id=\"flt-app\"]/div[2]/main[1]/div[3]/div/div[3]/div/div[2]/div[3]")).Click();
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sb_ifc50\"]/input")).Clear();
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sb_ifc50\"]/input")).SendKeys(searchItem.to);
            System.Threading.Thread.Sleep(500);
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"sbse0\"]")).Click();
            System.Threading.Thread.Sleep(500);
            slowDown();

            // Set Departure Date
            driver.FindElement(By.XPath("//*[@id=\"flt-app\"]/div[2]/main[1]/div[3]/div/div[3]/div/div[2]/div[5]/div[1]")).Click();
            slowDown();
            System.Threading.Thread.Sleep(500);
            driver.FindElement(By.XPath("//*[@id=\"flt-modaldialog\"]/div/div[4]/div[2]/div[1]/date-input/input")).SendKeys(searchItem.departDate.ToString("MM/dd/yyyy"));
            slowDown();

            // Set Return Date
            driver.FindElement(By.XPath("//*[@id=\"flt-modaldialog\"]/div/div[4]/div[2]/div[3]/date-input/input")).SendKeys(searchItem.returnDate.ToString("MM/dd/yyyy"));
            slowDown();

            // Maybe Future Functionality (One Way / Round Trip)
            driver.FindElement(By.XPath("//*[@id=\"flt-modaldialog\"]/div/div[4]/div[1]/div/div/dropdown-menu/div/div[1]/span[1]")).Click();
            slowDown();
            driver.FindElement(By.XPath("//*[@id=\"flt-modaldialog\"]/div/div[4]/div[1]/div/div/dropdown-menu/div/div[2]/menu-item[1]/span")).Click();
            slowDown();

            // Search
            driver.FindElement(By.XPath("//*[@id=\"flt-modaldialog\"]/div/div[5]/g-raised-button/div")).Click();
            slowDown();
            System.Threading.Thread.Sleep(5000);


            // The Good Part (Get Best Price)
            //*[@id="flt-app"]/div[2]/main[4]/div[7]/div[1]/div[4]/div[3]/div[2]/ol/li[1]/div/div[1]/div[2]/div[1]/div[1]/div[5]/div[1]
            string price = driver.FindElement(By.ClassName("gws-flights-results__cheapest-price")).Text;
            MongoDAL.LogSearchedPrice("Google Flights", searchItem, price);
            driver.Close();
            Console.WriteLine($"Done checking flight prices for trip '{searchItem.name}' on Google Flights.\n\n");
        }
        

        private static void slowDown()
        {
            if (sleepTime > 0)
            {
                System.Threading.Thread.Sleep(sleepTime);
            }
        }
    }
}
// //*[@id="flt-app"]/div[2]/main[1]/div[3]/div/div[3]/div/div[2]/div[1]

// //*[@id="sb_ifc50"]/input