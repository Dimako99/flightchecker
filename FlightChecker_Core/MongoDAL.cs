﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;
using System;
using System.Collections.Generic;
using FlightChecker.Models;

namespace FlightChecker
{
    public class MongoDAL
    {

        public static List<searchItem> GetSearchItems()
        {
            List<searchItem> searchItems = new List<searchItem>();
            MongoClient client = new MongoClient(ConfigurationSettings.AppSettings["MongoDbUrl"]);
            IMongoDatabase DB = client.GetDatabase("Trips");
            IMongoCollection<BsonDocument> collection = DB.GetCollection<BsonDocument>("trips");
            List<BsonDocument> list = collection.Find(new BsonDocument()).ToList();
            foreach(BsonDocument item in list)
            {
                Models.searchItem searchItem = new Models.searchItem() {
                    name = item["name"].AsString,
                    from = item["from"].AsString,
                    to = item["to"].AsString,
                    departDate = DateTime.Parse(item["departDate"].AsString),
                    returnDate = DateTime.Parse(item["returnDate"].AsString)
                };
                searchItems.Add(searchItem);
            }
            return searchItems;
        }

        public static void LogSearchedPrice(string website, searchItem searchItem, string price)
        {
            searchedItem searchedItem = new searchedItem() {
                name = searchItem.name,
                from = searchItem.from,
                to = searchItem.to,
                departDate = searchItem.departDate,
                returnDate = searchItem.returnDate,
                price = price,
                searchLocation = website,
                dateSearched = DateTime.Now
            };
            MongoClient client = new MongoClient(ConfigurationSettings.AppSettings["MongoDbUrl"]);
            IMongoDatabase DB = client.GetDatabase("FlightPrices");
            IMongoCollection<BsonDocument> collection = DB.GetCollection<BsonDocument>("flightPrices");
            collection.InsertOne(searchedItem.ToBsonDocument());
        }
    }
}
