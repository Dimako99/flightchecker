﻿using System;
using System.Collections.Generic;
using FlightChecker.Models;

namespace FlightChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing Flight Checker...");
            Console.WriteLine("Getting all trips that need to be checked");
            List<searchItem> searchItems = MongoDAL.GetSearchItems();
            Console.WriteLine($"Checking for flight prices for {searchItems.Count} trips");
            foreach(searchItem searchItem in searchItems)
            {
                WebCheck.checkGoogleFlights(searchItem);
            }
            Console.WriteLine("Done checking for flight prices");
            Console.WriteLine("Exiting Flight Checker");
        }
    }
}
